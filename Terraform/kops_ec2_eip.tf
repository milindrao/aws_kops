data "terraform_remote_state" "eip" {
  backend = "local"

  config = {
    path = "terraform-eip.tfstate"
  }
}

resource "aws_eip_association" "eip_assoc" {
  instance_id   = aws_instance.bastion.id
  allocation_id = "${data.terraform_remote_state.eip.outputs.bastion_eip_id}"
}

output "bastion_eip_ip" {
  value = "${data.terraform_remote_state.eip.outputs.bastion_eip_ip}"
}
