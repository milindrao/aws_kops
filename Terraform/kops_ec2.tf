data "aws_ami" "latest_ecs" {
  most_recent = true
  owners = ["amazon"]

  filter {
      name   = "name"
	  values = ["amzn2-ami-hvm-*"]
  }

  filter {
      name   = "virtualization-type"
      values = ["hvm"]
  }  
}

resource "aws_key_pair" "deployer" {
  key_name   = var.key_name
  public_key = "${file(var.public_key_file)}"
}

resource "aws_instance" "bastion" {
  ami                         = "${data.aws_ami.latest_ecs.id}"
  instance_type               = var.ec2_type
  key_name                    = "${aws_key_pair.deployer.key_name}"
  subnet_id                   = aws_subnet.public.id
  vpc_security_group_ids      = ["${aws_security_group.kops_bastion_sg.id}"]
  iam_instance_profile        = "${aws_iam_instance_profile.kops_role_profile.name}"
  associate_public_ip_address = true
  
  depends_on = [aws_s3_bucket.kops_s3_bucket]  

#  user_data = <<EOF
#		#! /bin/bash
#		sudo hostnamectl set-hostname kops_bh
#  EOF


  # Copy in the bash script we want to execute.
  # The source is the location of the bash script on the local PC you are executing terraform from
  # The destination is on the new AWS instance.
  provisioner "file" {
    source      = "bootstrap_files/"
    destination = "~"
	
	connection {
      type        = "ssh"
      user        = "ec2-user"
      private_key = "${file(var.pem_file)}"
      host        = "${self.public_dns}"
    }	
  }  
  
  provisioner "file" {
    source      = "cluster_config"
    destination = "~"
	
	connection {
      type        = "ssh"
      user        = "ec2-user"
      private_key = "${file(var.pem_file)}"
      host        = "${self.public_dns}"
    }	
  }  
  
  # Change permissions on bash script and execute from ec2-user.
  provisioner "remote-exec" {
    inline = [
      "chmod +x ~/*.sh",
      "~/bootstrap.sh ${var.s3_bucket_name} ${var.cluster_zones}",
    ]

	connection {
      type        = "ssh"
      user        = "ec2-user"
      private_key = "${file(var.pem_file)}"
      host        = "${self.public_dns}"
    }	
  }  
  
  tags = {
    Name = "${var.ec2_tag_name} Bastion Host"
  }
}

output "bastion_instance_dns" {
  value = aws_instance.bastion.public_ip
}
