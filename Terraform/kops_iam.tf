# EC2 role
resource "aws_iam_role" "kops_role" {
  name = "kops_role"

  # Terraform's "jsonencode" function converts a
  # Terraform expression result to valid JSON syntax.
  assume_role_policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Action = "sts:AssumeRole"
        Effect = "Allow"
        Principal = {
          Service = "ec2.amazonaws.com"
        }
      },
    ]
  })

  managed_policy_arns = ["arn:aws:iam::aws:policy/AmazonEC2FullAccess", "arn:aws:iam::aws:policy/AmazonRoute53FullAccess", "arn:aws:iam::aws:policy/AmazonS3FullAccess", "arn:aws:iam::aws:policy/IAMFullAccess", "arn:aws:iam::aws:policy/AmazonVPCFullAccess", "arn:aws:iam::aws:policy/AWSCloudFormationReadOnlyAccess"]

  tags = {
    Name = "${var.ec2_tag_name} Kops Role"
  }
}

# EC2 Instance Profile
resource "aws_iam_instance_profile" "kops_role_profile" {
  name = "kops_role_profile"
  role = "${aws_iam_role.kops_role.name}"
}

