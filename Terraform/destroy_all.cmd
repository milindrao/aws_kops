@REM Run Terraform to destroy the kops BH and the EIP
@echo off

setlocal

REM del tf.log
REM set TF_LOG=DEBUG
REM set TF_LOG_PATH=tf.log

call runtf destroy
cd elastic_ip
call runtf destroy
cd..

endlocal
