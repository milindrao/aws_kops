resource "aws_s3_bucket" "kops_s3_bucket" {
  bucket = var.s3_bucket_name
  force_destroy = true

  tags = {
    Name = "${var.ec2_tag_name} s3_bucket"
  }
}