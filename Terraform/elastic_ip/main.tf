variable "profile_name" {
}  

variable "region_name" {
}  

variable "ec2_tag_name" {
}  

provider "aws" {
  region  = var.region_name
  profile = var.profile_name
}

terraform {                                                                                                             
  backend "local" {                                                                                                     
    path = "../terraform-eip.tfstate"                                                                                   
  }                                                                                                                     
}                                                                                                                       

# EIP
resource "aws_eip" "bastion_eip" {
  vpc = true

#  lifecycle {
#    prevent_destroy = true
#  }

  tags = {
    Name = "${var.ec2_tag_name} Bastion EIP"
	
  }
}

output "bastion_eip_id" {
  value = "${aws_eip.bastion_eip.id}"
}

output "bastion_eip_ip" {
  value = "${aws_eip.bastion_eip.public_ip}"
}

