alias cls=clear
alias ra='source ~/.bash_aliases'
alias nal='nano ~/.bash_aliases'
alias k=kubectl
alias kga='kubectl get all'
alias kl='kubectl logs -f'
alias kd='kubectl describe'
alias kpuc='kops update cluster ${NAME} --yes --admin=87600h'
alias kpdc='kops delete cluster ${NAME} --yes'
alias kpvc='kops validate cluster --name ${NAME}'

kit()
{
   kubectl -it exec $1 -- sh
}
kg()
{
   kubectl get $1
}

kgw()
{
   kubectl get $1 -o wide
}