#! /bin/bash
# Setup kops. 
# Expected Arguments: 
#    $1 comma separated AZs (e.g. us-east-1a,us-east-1b,us-east-1c)

if [[ $1 == '' ]]; then 
	echo "Error: Expected comma separated AZs as an argument"
	exit -1
fi

# Set the volume sizes
nodeVolumeSize=40
masterVolumeSize=20
etcdVolumeSize=10

# Function to update the cluster size and disk size of the node instance groups of the specified AZs
function update_node_instance_groups()
{
	IFS=',' read -r -a zones <<< $1
	for zone in "${zones[@]}"
	do
		kops get ig nodes-$zone -o yaml --name ${NAME} > nodes-$zone.yaml
		sed -i "s/maxSize:.*/maxSize: $max_size/" nodes-$zone.yaml
		sed -i "s/minSize:.*/minSize: $min_size/" nodes-$zone.yaml
		echo "  rootVolumeSize: $nodeVolumeSize" >> nodes-$zone.yaml
		kops replace -f nodes-$zone.yaml --name ${NAME}
		rm nodes-$zone.yaml
	done
}

# Function to update the disk size of the master instance group
function update_master_instance_group()
{
	IFS=',' read -r -a zones <<< $1
	zone=${zones[0]}
	kops get ig master-$zone -o yaml --name ${NAME} > master-$zone.yaml
	echo "  rootVolumeSize: $masterVolumeSize" >> master-$zone.yaml
	kops replace -f master-$zone.yaml --name ${NAME}
	rm master-$zone.yaml
}

# Function to update the etcd disk size
function update_etcd_cluster()
{
	IFS=',' read -r -a zones <<< $1
	zone=${zones[0]}
	kops get cluster -o yaml --name ${NAME} > cluster_master-$zone.yaml
	sed -i "s/- instanceGroup: master-$zone/- instanceGroup: master-$zone\n      volumeSize: $etcdVolumeSize/" cluster_master-$zone.yaml
	kops replace -f cluster_master-$zone.yaml --name ${NAME}
	rm cluster_master-$zone.yaml
}

# Generate the keypair
ssh-keygen -b 2048 -t rsa -f ~/.ssh/id_rsa -N ''

# Configure cluster
kops create cluster ${NAME} --zones=$1
kops create secret --name ${NAME} sshpublickey admin -i ~/.ssh/id_rsa.pub
kops export kubecfg --name ${NAME} --admin=87600h
update_etcd_cluster $1

# Update the instance group configuration
max_size=3
min_size=2
update_node_instance_groups $1
update_master_instance_group $1

# Update the cluster and export the configuration
kops update cluster ${NAME} --yes --admin=87600h
kops export kubecfg --name ${NAME} --admin=87600h
