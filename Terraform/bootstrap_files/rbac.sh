#! /bin/bash
# Create new user and setup RBAC

# fail fast
#set -e

# Create the playground namespace
kubectl create ns playground

user=francis-linux-login-name
#user=test
s3_bucket=milind-kops-state-storage

# Generate the key and certs for the user
key=$(aws s3 ls s3://$s3_bucket/fleetman.k8s.local/pki/private/ca/ | grep '\.key$')
key="$(cut -d' ' -f4 <<< $key)"
aws s3 cp s3://$s3_bucket/fleetman.k8s.local/pki/private/ca/$key kubernetes.key
crt=$(aws s3 ls s3://$s3_bucket/fleetman.k8s.local/pki/issued/ca/ | grep '\.crt$')
crt="$(cut -d' ' -f4 <<< $crt)"
aws s3 cp s3://$s3_bucket/fleetman.k8s.local/pki/issued/ca/$crt kubernetes.crt
openssl genrsa -out $user-private.key 2048
openssl req -new -key $user-private.key -out req.csr -subj "/CN=$user/O=$user"
openssl x509 -req -in req.csr -CA kubernetes.crt -CAkey kubernetes.key -CAcreateserial -out $user.crt -days 365
chmod 400 *.key

# create the user
sudo useradd -p $(openssl passwd -1 "password") $user

# Copy the key and certs to the user's home directory
sudo mkdir /home/$user/.certs
sudo mv {$user.crt,$user-private.key,kubernetes.crt} /home/$user/.certs
sudo chown -R $user:$user /home/$user/.certs
sudo ls -l /home/$user/.certs
rm -f kubernetes.key kubernetes.srl req.csr

# setup kubernetes config
url=https:$(kubectl config view | grep server: | cut -d: -f3)
echo $url
sudo runuser -l $user -c "kubectl config set-cluster fleetman.k8s.local --server $url"
sudo runuser -l $user -c "kubectl config set-context mycontext --cluster fleetman.k8s.local --user $user"
sudo runuser -l $user -c "kubectl config use-context mycontext"
sudo runuser -l $user -c "kubectl config set-credentials $user --client-certificate=.certs/$user.crt --client-key=.certs/$user-private.key"
sudo runuser -l $user -c "kubectl config set-cluster fleetman.k8s.local --certificate-authority=.certs/kubernetes.crt"
sudo runuser -l $user -c "kubectl config view"
