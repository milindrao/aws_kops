#! /bin/bash
# Bootstrap the server to setup kops
# Expected Arguments: 
#    $1 The s3 storage bucket name
#    $2 comma separated AZs (e.g. us-east-1a,us-east-1b,us-east-1c)

if [[ $1 == '' ]]; then 
	echo "Error: Expected S3 Bucket name for storage"
	exit -1
fi

if [[ $2 == '' ]]; then 
	echo "Error: Expected comma separated AZs as an argument"
	exit -1
fi

# set aliases and environment variables
echo 'source ~/.bash_aliases' >> ~/.bashrc
echo 'export EDITOR=nano' >> ~/.bashrc
echo 'export NAME=fleetman.k8s.local' >> ~/.bashrc
echo "export KOPS_STATE_STORE=s3://$1" >> ~/.bashrc
source ~/.bashrc

# Install kops
curl -Lo kops https://github.com/kubernetes/kops/releases/download/$(curl -s https://api.github.com/repos/kubernetes/kops/releases/latest | grep tag_name | cut -d '"' -f 4)/kops-linux-amd64
chmod +x kops
sudo mv kops /usr/local/bin/kops

# Install kubectl
curl -LO "https://dl.k8s.io/release/$(curl -L -s https://dl.k8s.io/release/stable.txt)/bin/linux/amd64/kubectl"
sudo install -o root -g root -m 0755 kubectl /usr/local/bin/kubectl

# Setup kops
~/setup_kops.sh $2