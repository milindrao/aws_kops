variable "profile_name" {
}  

variable "region_name" {
}  

variable "cluster_zones" {
}  

variable "ec2_type" {
  default = "t2.micro"
}

variable "s3_bucket_name" {
}

variable "key_name" {
}

variable "public_key_file" {
}  

variable "pem_file" {
}  

variable "ec2_tag_name" {
}

variable "vpc_cidr" {
}

variable "vpc_public_cidr" {
}

variable "vpc_private_cidr" {
}

variable "authorized_cidr" {
}

