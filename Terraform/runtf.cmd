@REM Run Terraform Action Batch file
@echo off

setlocal
set action=plan
set target=

REM del tf.log
REM set TF_LOG=DEBUG
REM set TF_LOG_PATH=tf.log

if not [%1] == [] (
	set action=%1
)

if not [%2] == [] (
	set target=-target %2
)

if [%action%] == [apply] set auto-approve=1
if [%action%] == [destroy] set auto-approve=1

if defined auto-approve (
	terraform %action% %target% -var-file kops.tfvars --auto-approve 
) else (
	terraform %action% %target% -var-file kops.tfvars
)

