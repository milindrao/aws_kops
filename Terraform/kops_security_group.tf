resource "aws_security_group" "kops_bastion_sg" {
  name        = "bastion_sg"
  description = "Bastion Host for kops"
  vpc_id      = aws_vpc.main.id

  ingress {
    protocol    = "tcp"
    from_port   = 22
    to_port     = 22
    cidr_blocks = ["${var.authorized_cidr}"]
  }

  ingress {
    from_port   	= -1
    to_port     	= -1
    protocol    	= "icmp"
    self		 	= true
  }

  egress {
    protocol    = -1
    from_port   = 0
    to_port     = 0
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
	Name = "${var.ec2_tag_name} Bastion Host SG"
  }

}
