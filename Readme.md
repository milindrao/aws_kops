# Create Kubernetes Cluster using Kops
Creates a Kubernetes cluster and deploys an application

## Pre-requesites
 * Terraform is installed
 
## Creating the cluster
 * Clone the repository into a folder, and Run terraform init from the Terraform folder.  
```
  cd Terraform  
  terraform init  
  cd elastic_ip  
  terraform init  
  cd..  
```
 * Set the AWS configuration.  Edit the kops.tfvars file.  See below
 * Apply terraform  
   Apply the terraform file in the elastic_ip folder to create an Elastic IP and then apply the TF files in the project to create a bastion host and kick off the creation of the K8s cluster from the BH
   ```
   apply_all.cmd
   ```
 * SSH into the Bastion Host using the elastic IP address to create the cluster  
   Run the script to create the cluster. After the script is run, it takes about 5 minutes for the instances to show up in the AWS Console in the instances link on EC2
```
  ~/setup_kops.sh <cluster_zones>  
  e.g. ~/setup_kops.sh us-east-1a,us-east-1b,us-east-1c
```
 * *After* all the instances show up as initialized in AWS Console, validate the cluster is up and running.
   ```
   kops validate cluster
   ```
 * Deploy the application in the k8s cluster
   On the bastion host, after the cluster has been created
   ```
   ~/deploy.sh
   ```
   
### tfvars file
   | Property         | Description |
   |------------------|-------------|
   | region_name      | AWS Regions to be used |
   | profile name     | Name of the profile to be used. Created using aws configure --profile <profile name> |
   | key_name         | The name of the key pair for SSHing into the Bastion Host|
   | public_key_file  | The public key of the key pair |
   | pem_file         | The pem file of the key pair|
   | s3_bucket_name   | Name of the S3 bucket for storing the master node config data |
   | vpc_cidr         | The VPC in which to create the BH  |
   | vpc_public_cidr  | The public subnet in the VPC |
   | vpc_private_cidr | The private subnet of the VPC |
   | authorized_cidr  | The CIDR from which to SSH into the Bastion Host |
   
## Destroying the cluster
On the bastion host, 
```
kops delete cluster ${NAME} --yes
```

## Destroying the Servers on AWS
Destroy the VPC, Elastic IP and Bastion Host 
```
destroy_all.cmd
```
